﻿export default [
  {
    path: '/admin/login',
    layout: false,
    hideInMenu: true,
    name: 'login',
    component: './user/Login/adminlogin',
  },
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user/login',
        layout: false,
        name: 'login',
        component: './user/Login',
      },

      {
        path: '/user',
        redirect: '/user/login',
      },
      {
        name: 'register',
        icon: 'smile',
        path: '/user/register',
        component: './user/register',
      },
      {
        component: '404',
      },
    ],
  },
  {
    hideInMenu: true,
    name: 'account',
    icon: 'user',
    path: '/account',
    routes: [
      {
        name: 'center',
        icon: 'smile',
        path: '/account/center',
        component: './account/center',
      },
    ],
  },

  {
    name: 'QuanLyDanhMuc',
    icon: 'OrderedListOutlined',
    path: '/quanlydanhmuc',
    routes: [
      {
        name: 'NamThanhToan',
        path: './namthanhtoan',
        component: './NamThanhToan',
      },
      {
        name: 'NghiaVuMacDinh',
        path: './nghiavumacdinh',
        component: './NghiaVuMacDinh',
      },
      {
        name: 'DonGia',
        path: './dongia',
        component: './DonGiaThanhToan',
      },
      {
        name: 'ChucVu',
        path: './chucvu',
        component: './DanhMucUser/ChucVu',
      },
      {
        name: 'GioNCKH',
        path: './gionckh',
        component: './DanhMucUser/GioNCKH',
      },
      {
        name: 'GioTienTamUng',
        path: './giotientamung',
        component: './DanhMucUser/TienTamUng',
      },
      {
        name: 'DinhMucHuongDanDaiHoc',
        path: './dinhmuchuongdandaihoc',
        component: './DinhMucHuongDanDaiHoc',
      },
      {
        name: 'DinhMucHuongDanSauDaiHoc',
        path: './dinhmuchuongdansaudaihoc',
        component: './DinhMucHuongDanSauDaiHoc',
      },
      {
        name: 'DinhMucHuongDanKhac',
        path: './dinhmuchuongdankhac',
        component: './DinhMucHuongDanKhac',
      },
      {
        name: 'DanhMucThanhToan',
        path: './danhmucthanhtoan',
        component: './DanhMucThanhToan',
      },
      {
        name: 'CongViec',
        path: './congviec',
      },
      {
        name: 'ChucVuQuanLy',
        path: './chucvuquanly',
        component: './ChucVuQuanLy',
      },
      {
        name: 'ThanhToan',
        path: './thanhtoan',
        component: './ThanhToan'
      },
      {
        name: 'Dashboard',
        path: './dashboard',
        component: './DashBoard'
      }
    ],
  },

  {
    path: '/',
    redirect: '/user/login',
  },
  {
    component: '404',
  },
];
