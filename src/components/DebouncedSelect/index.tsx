import { ESystemRole } from '@/utils/constants';
import rules from '@/utils/rules';
import { Select, Spin } from 'antd';
import _ from 'lodash';
import { useEffect, useState } from 'react';
import { useModel } from 'umi';
import { FormItem } from '../FormItem';

const DebouncedSelect = (props: { initialValue?: any }) => {
  const { danhSach, getModel, loading } = useModel('user');
  const [keyword, setKeyword] = useState<any>('');
  useEffect(() => {
    getModel({ vai_tro: ESystemRole.nhan_vien, name: keyword }, 'pageable');
  }, [keyword]);

  const handleSearch = _.debounce((value) => {
    setKeyword(value);
  }, 800);
  return (
    <FormItem
      label="Giảng viên"
      name={'maGv'}
      initialValue={props?.initialValue}
      rules={[...rules.required]}
    >
      <Select
        // labelInValue
        filterOption={false}
        showSearch
        notFoundContent={loading ? <Spin size="small" /> : null}
        onSearch={(val) => handleSearch(val)}
        placeholder="Nhập tên giảng viên để tìm kiếm"
        options={danhSach.map((item) => ({
          label: `${item.ma_dinh_danh} - ${item.name}`,
          value: item.ma_dinh_danh,
        }))}
      />
    </FormItem>
  );
};

export default DebouncedSelect;
