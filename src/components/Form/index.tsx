import type { FormProps as FormPropsBase } from 'antd';
import { Button, Card, Form } from 'antd';
import type { FormProps } from 'rc-field-form/lib/Form';
import type { ReactElement } from 'react';
import { useModel } from 'umi';

const FormBase = (props: {
  modelName: any;
  children: ReactElement;
  hideCard?: boolean;
  otherProps: FormProps & FormPropsBase;
}) => {
  const { modelName, children, hideCard, otherProps } = props;
  const [form] = Form.useForm();
  const { edit, setVisibleForm, loading } = useModel(modelName);
  const mainContent = (
    <Form form={form} {...otherProps}>
      {children}
      <Form.Item style={{ textAlign: 'center', marginBottom: 0 }}>
        <Button style={{ marginRight: 8 }} htmlType="submit" type="primary">
          Lưu
        </Button>

        <Button onClick={() => setVisibleForm(false)}>Đóng</Button>
      </Form.Item>
    </Form>
  );

  if (hideCard) return mainContent;
  return (
    <Card loading={loading} title={`${edit ? 'Chỉnh sửa' : 'Thêm mới'}`}>
      {mainContent}
    </Card>
  );
};

export default FormBase;
