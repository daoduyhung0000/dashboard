import { MenuOutlined } from '@ant-design/icons';
import { Button, Divider, Popconfirm, Popover, Tooltip } from 'antd';
import type { ButtonProps } from 'antd/lib/button/button';

const ToolColumn = (props: {
  hasEdit?: ButtonProps;
  hasDelete?: {
    handleDelete: Function;
    props?: ButtonProps;
  };
  hasView?: ButtonProps;
  type: 'normal' | 'popover';
}) => {
  const { hasDelete, hasEdit, hasView, type } = props;

  const mainContent = (
    <>
      {hasEdit && (
        <Tooltip title="Chỉnh sửa">
          <Button shape="circle" {...hasEdit} />
        </Tooltip>
      )}
      {hasEdit && hasDelete && <Divider type="vertical" />}
      {hasDelete && (
        <Popconfirm title={'Bạn có chắc chắn xóa?'} onConfirm={() => hasDelete?.handleDelete()}>
          <Tooltip title="Xóa">
            <Button shape="circle" {...hasDelete?.props} />
          </Tooltip>
        </Popconfirm>
      )}
      {hasDelete && hasView && <Divider type="vertical" />}
      {hasView && (
        <Tooltip title="Chi tiết">
          <Button shape="circle" {...hasView} />
        </Tooltip>
      )}
    </>
  );
  if (type === 'normal') return mainContent;
  return (
    <Popover content={mainContent}>
      <Button icon={<MenuOutlined />} type="primary" />
    </Popover>
  );
};

export default ToolColumn;
