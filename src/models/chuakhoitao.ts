import useInitModel from "@/hooks/useInitModel"
import { useState } from "react"

export default () => {
    const [record, setRecord] = useState<ChuaKhoiTao.Record>()
    const [danhSach, setDanhSach] = useState<ChuaKhoiTao.Record[]>([])
    const objInitModel = useInitModel('ho-so-thanh-toan','cond', setDanhSach, setRecord)
    return {
        record,
        danhSach,
        ...objInitModel
    }
}