import useInitModel from '@/hooks/useInitModel';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<ChucVuQuanLy.Record>();
  const [danhSach, setDanhSach] = useState<ChucVuQuanLy.Record[]>([]);
  const objInitModel = useInitModel('chuc-vu-quan-ly', 'condition', setDanhSach, setRecord);
  return {
    record,
    setRecord,
    danhSach,
    setDanhSach,
    ...objInitModel,
  };
};
