import useInitModel from '@/hooks/useInitModel';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<DanhMucThanhToan.Record>();
  const [danhSach, setDanhSach] = useState<DanhMucThanhToan.Record[]>([]);

  const objInitModel = useInitModel('danh-muc-thanh-toan', 'condition', setDanhSach, setRecord);
  return {
    record,
    danhSach,
    setDanhSach,
    setRecord,
    ...objInitModel,
  };
};
