import useInitModel from '@/hooks/useInitModel';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<DanhMucUser.Record>();
  const [danhSach, setDanhSach] = useState<DanhMucUser.Record[]>([]);
  const objInitModel = useInitModel(
    'danh-muc-user/user-chuc-vu',
    'condition',
    setDanhSach,
    setRecord,
  );

  return {
    record,
    setRecord,
    danhSach,
    setDanhSach,
    ...objInitModel,
  };
};
