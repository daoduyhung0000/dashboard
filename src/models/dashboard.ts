import { getAllSocialInteractive, getLikeInteractive, getSocialValue } from "@/services/DashBoard/interactive"
import { useState } from "react"

export default () => {
    const [danhSach, setDanhSach] = useState<SocialInteractive.Record[]>([])
    const [followersChart, setFollowersCharts] = useState<{type: string, value: string}[]>([])
    const [subscribersChart, setSubscribersChart] = useState<{type: string, value: string}[]>([])
    const [likeChart, setLikeChart] = useState<{type: string, value: string}[]>([])
    const [rank, setRank] = useState<Rank.Record>()
    const [trustFollowerPercent, setTrustFollowerPercent] = useState<number>(0)
    const [likeInteractive, setLikeInteractive] = useState([])
    const [arrLike, setArrLike] = useState<number[]>([])
    async function getAllDanhSach() {
        const res = await getAllSocialInteractive()
        setDanhSach(res?.data)
        setSubscribersChart(res?.data.map((data: SocialInteractive.Record) => {
            return {
                type: data?.name,
                value: data?.subscribers
            }
        }))
        setFollowersCharts(res?.data.map((data: SocialInteractive.Record) => {
            return {
                type: data?.name,
                value: data?.followers
            }
        }))
        setLikeChart(res?.data.map((data: SocialInteractive.Record) => {
            return {
                type: data?.name,
                value: data?.like
            }
        }))
        const trustFollower = res?.data.reduce((pre: number, cur: SocialInteractive.Record ) => pre + cur.trustFollower, 0)
        const trueFollower = res?.data.reduce((pre: number, cur: SocialInteractive.Record) => pre + cur.followers, 0)
        const total = trustFollower / trueFollower 
        setTrustFollowerPercent(total)
    }
    async function getRank(){
        const res = await getSocialValue()
        setRank(res?.data[0])
    }
    async function getLike(){
        const res = await getLikeInteractive()
        const arr = res?.data.reduce((pre: number[], cur: Like.Record) => pre.concat(cur.khoang), [])
        setLikeInteractive(res?.data)
        setArrLike(arr)
    }
    return {
        getAllDanhSach,
        danhSach,
        setDanhSach,
        followersChart,
        setFollowersCharts,
        subscribersChart,
        setSubscribersChart,
        likeChart, 
        setLikeChart,
        rank,
        setRank,
        getRank,
        trustFollowerPercent,
        likeInteractive, 
        setLikeInteractive,
        getLike,
        arrLike,
        setArrLike
    }
}