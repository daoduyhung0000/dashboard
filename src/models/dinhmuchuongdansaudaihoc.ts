import useInitModel from '@/hooks/useInitModel';
import type { DinhMuc } from '@/services/DinhMuc/typings';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<DinhMuc.Record>();
  const [danhSach, setDanhSach] = useState<DinhMuc.Record[]>([]);
  const objInitModel = useInitModel(
    'huong-dan-sau-dh-dinh-muc',
    'condition',
    setDanhSach,
    setRecord,
  );

  return {
    record,
    setRecord,
    danhSach,
    setDanhSach,
    ...objInitModel,
  };
};
