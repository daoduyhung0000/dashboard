import useInitModel from '@/hooks/useInitModel';
import type { DonGiaThanhToan } from '@/services/DonGiaThanhToan/typings';
import { EHeDaoTao } from '@/utils/constants';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<DonGiaThanhToan.Record>();
  const [danhSach, setDanhSach] = useState<DonGiaThanhToan.Record[]>([]);
  const [heDaoTao, setHeDaoTao] = useState<EHeDaoTao>(EHeDaoTao.DAI_HOC_CHINH_QUY);
  const objInitModel = useInitModel('don-gia-thanh-toan', 'condition', setDanhSach, setRecord);
  return {
    record,
    setRecord,
    danhSach,
    setDanhSach,
    heDaoTao,
    setHeDaoTao,
    ...objInitModel,
  };
};
