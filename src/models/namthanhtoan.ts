import useInitModel from '@/hooks/useInitModel';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<NamThanhToan.Record>();
  const [danhSach, setDanhSach] = useState<NamThanhToan.Record[]>([]);
  const objInitModel = useInitModel('nam-thanh-toan', 'condition', setDanhSach, setRecord);

  return {
    record,
    setRecord,
    danhSach,
    setDanhSach,
    ...objInitModel,
  };
};
