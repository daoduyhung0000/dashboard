import useInitModel from '@/hooks/useInitModel';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<NghiaVuMacDinh.Record>();
  const [danhSach, setDanhSach] = useState<NghiaVuMacDinh.Record[]>([]);
  const objInitModel = useInitModel('nghia-vu-mac-dinh', 'condition', setDanhSach, setRecord);
  return {
    record,
    setRecord,
    danhSach,
    setDanhSach,
    ...objInitModel,
  };
};
