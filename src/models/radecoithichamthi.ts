import useInitModel from '@/hooks/useInitModel';
import type { RaDeCoiThiChamThi } from '@/services/RaDeCoiThiChamThi/typings';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<RaDeCoiThiChamThi.Record>();
  const [danhSach, setDanhSach] = useState<RaDeCoiThiChamThi.Record[]>([]);
  const objInitModel = useInitModel('ra-de-coi-thi-cham-thi', 'condition', setDanhSach, setRecord);
  return {
    record,
    setRecord,
    danhSach,
    setDanhSach,
    ...objInitModel,
  };
};
