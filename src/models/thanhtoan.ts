/* eslint-disable @typescript-eslint/no-unused-expressions */

import { getNamThanhToan } from "@/services/ThanhToan/ThanhToan"
import { useState } from "react"


export default () => {
    const [danhSachNamThanhToan, setDanhSachNamThanhToan] = useState<NamThanhToan.Record[]>([])
    const [checkNamThanhToanSuccess, setCheckNamThanhToanSuccess] = useState<boolean>(false)
    const [isNamThanhToanErr, setIsNamThanhToanErr] = useState<boolean>(false)
    const [namActive, setNamActive] = useState<number>(0)
    const [activeApi, setActiveApi] = useState<string>('/pageable/chua-khoi-tao')
    const getDanhSachNamThanhToan = async () => {
        const res = getNamThanhToan()
        res.then((arr) => {
                setDanhSachNamThanhToan(arr?.data?.data)
                setCheckNamThanhToanSuccess(true)
                setNamActive(arr?.data?.data[arr?.data?.data.length-1].id)
                
        })
        .catch((err) => {
            console.log(err)
            setIsNamThanhToanErr(true)
            setCheckNamThanhToanSuccess(true)
        })
    }
    return {
        danhSachNamThanhToan,
        setDanhSachNamThanhToan,
        getDanhSachNamThanhToan,
        checkNamThanhToanSuccess, 
        setCheckNamThanhToanSuccess,
        isNamThanhToanErr, 
        setIsNamThanhToanErr,
        namActive, 
        setNamActive,
        activeApi,
        setActiveApi
    }
}