import useInitModel from '@/hooks/useInitModel';
import type { User } from '@/services/User/typings';
import { useState } from 'react';

export default () => {
  const [record, setRecord] = useState<User.Record>();
  const [danhSach, setDanhSach] = useState<User.Record[]>([]);
  const objInitModel = useInitModel('odoo-user', 'condition', setDanhSach, setRecord);
  return {
    record,
    setRecord,
    danhSach,
    setDanhSach,
    ...objInitModel,
  };
};
