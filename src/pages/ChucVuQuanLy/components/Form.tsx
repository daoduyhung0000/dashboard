import FormBase from '@/components/Form';
import rules from '@/utils/rules';
import { Input, InputNumber, Radio } from 'antd';
import FormItem from 'antd/es/form/FormItem';
import { useModel } from 'umi';

const FormChucVu = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('chucvuquanly');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <>
      <FormItem
        label="Tên chức vụ"
        name={'ten'}
        initialValue={record?.ten}
        rules={[...rules.required, ...rules.text]}
      >
        <Input placeholder="Tên chức vụ" />
      </FormItem>
      <FormItem
        label="Định mức"
        name={'dinhMuc'}
        initialValue={record?.dinhMuc}
        rules={[...rules.required]}
      >
        <InputNumber style={{ width: '100%' }} placeholder="Định mức" min={0} />
      </FormItem>
      <FormItem
        label="Được thanh toán toàn bộ giờ giảng"
        name={'tinhToanBoGioGiang'}
        initialValue={record?.tinhToanBoGioGiang ?? false}
        rules={[...rules.required]}
      >
        <Radio.Group
          options={[
            { label: 'Có', value: true },
            {
              label: 'Không',
              value: false,
            },
          ]}
        />
      </FormItem>
    </>
  );

  return (
    <FormBase modelName="chucvuquanly" otherProps={{ onFinish: onSubmit, labelCol: { span: 24 } }}>
      {content}
    </FormBase>
  );
};

export default FormChucVu;
