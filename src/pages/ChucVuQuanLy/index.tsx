import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { IColumn } from '@/utils/interfaces';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import FormChucVu from './components/Form';

const ChucVuQuanLyComponent = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('chucvuquanly');
  const getData = () => {
    getModel(undefined, 'pageable');
  };
  const handleEdit = (record: ChucVuQuanLy.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<ChucVuQuanLy.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Tên chức vụ',
      dataIndex: 'ten',
      align: 'center',
    },
    {
      title: 'Định mức',
      dataIndex: 'dinhMuc',
      align: 'center',
      width: 120,
    },
    {
      title: 'Được thanh toán toàn bộ giờ giảng',
      dataIndex: 'tinhToanBoGioGiang',
      align: 'center',
      width: 200,
      render: (val) => (val ? 'Có' : 'Không'),
    },

    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: ChucVuQuanLy.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Chức vụ quản lý"
      hascreate
      modelName="chucvuquanly"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      Form={() => <FormChucVu getData={getData} />}
    />
  );
};

export default ChucVuQuanLyComponent;
