import FormBase from '@/components/Form';
import rules from '@/utils/rules';
import { Form, Input } from 'antd';
import { useModel } from 'umi';

const FormDanhMucThanhToan = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('danhmucthanhtoan');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <>
      <Form.Item
        label="Tên"
        name={'name'}
        initialValue={record?.name}
        rules={[...rules.required, ...rules.text]}
      >
        <Input placeholder="Tên" />
      </Form.Item>
      <Form.Item
        label="Giá trị"
        name={'value'}
        initialValue={record?.value}
        rules={[...rules.required, ...rules.text]}
      >
        <Input placeholder="Giá trị" />
      </Form.Item>
      <Form.Item
        label="Loại"
        name={'type'}
        initialValue={record?.type}
        rules={[...rules.required, ...rules.text]}
      >
        <Input placeholder="Loại" />
      </Form.Item>
    </>
  );

  return (
    <FormBase
      modelName="danhmucthanhtoan"
      otherProps={{ onFinish: onSubmit, labelCol: { span: 4 }, labelAlign: 'left' }}
    >
      {content}
    </FormBase>
  );
};

export default FormDanhMucThanhToan;
