import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { IColumn } from '@/utils/interfaces';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import FormDanhMucThanhToan from './components/Form';

const DanhMucThanhToan = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('danhmucthanhtoan');
  const getData = () => {
    getModel(undefined, 'pageable');
  };
  const handleEdit = (record: DanhMucThanhToan.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<DanhMucThanhToan.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      align: 'center',
    },
    {
      title: 'Giá trị',
      dataIndex: 'value',
      align: 'center',
    },
    {
      title: 'Loại',
      dataIndex: 'type',
      align: 'center',
    },
    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: DanhMucThanhToan.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Danh mục thanh toán"
      hascreate
      modelName="danhmucthanhtoan"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      Form={() => <FormDanhMucThanhToan getData={getData} />}
    />
  );
};

export default DanhMucThanhToan;
