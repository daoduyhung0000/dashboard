import DebouncedSelect from '@/components/DebouncedSelect';
import FormBase from '@/components/Form';
import rules from '@/utils/rules';
import { InputNumber, Select } from 'antd';
import FormItem from 'antd/es/form/FormItem';
import { useModel } from 'umi';

const FormChucVu = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('danhmucuserchucvu');
  const { danhSach } = useModel('chucvuquanly');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <>
      <FormItem label="Năm học" name={'nam'} initialValue={record?.nam} rules={[...rules.required]}>
        <InputNumber
          min={2010}
          max={new Date().getFullYear() + 2}
          style={{ width: '100%' }}
          placeholder="Năm học"
        />
      </FormItem>

      <DebouncedSelect initialValue={record?.maGv} />

      <FormItem
        label="Chức vụ"
        name={'tenChucVu'}
        initialValue={record?.tenChucVu}
        rules={[...rules.required]}
      >
        <Select
          placeholder="Chức vụ"
          options={danhSach.map((item) => ({ label: item.ten, value: item.ten }))}
        />
      </FormItem>
    </>
  );

  return (
    <FormBase
      modelName="danhmucuserchucvu"
      otherProps={{ onFinish: onSubmit, labelCol: { span: 24 } }}
    >
      {content}
    </FormBase>
  );
};

export default FormChucVu;
