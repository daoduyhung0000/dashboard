import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { IColumn } from '@/utils/interfaces';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useEffect } from 'react';
import { useModel } from 'umi';
import FormChucVu from './components/Form';

const ChucVuQuanLyComponent = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('danhmucuserchucvu');

  const { getAllModel } = useModel('chucvuquanly');

  useEffect(() => {
    getAllModel();
  }, []);

  const getData = () => {
    getModel(undefined, 'pageable');
  };
  const handleEdit = (record: DanhMucUser.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<DanhMucUser.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Năm học',
      dataIndex: 'nam',
      align: 'center',
    },
    {
      title: 'Giảng viên',
      dataIndex: 'maGv',
      align: 'center',
      // width: 120,
    },
    {
      title: 'Tên chức vụ',
      dataIndex: 'tenChucVu',
      align: 'center',
      // width: 200,
    },

    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: DanhMucUser.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Quản lý chức vụ"
      hascreate
      modelName="danhmucuserchucvu"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      Form={() => <FormChucVu getData={getData} />}
    />
  );
};

export default ChucVuQuanLyComponent;
