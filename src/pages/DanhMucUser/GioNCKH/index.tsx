import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { IColumn } from '@/utils/interfaces';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import FormGioNCKH from './components/Form';

const GioNCKHComponent = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('danhmucusergionckh');

  const getData = () => {
    getModel(undefined, 'pageable');
  };
  const handleEdit = (record: DanhMucUser.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<DanhMucUser.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Năm học',
      dataIndex: 'nam',
      align: 'center',
    },
    {
      title: 'Giảng viên',
      dataIndex: 'maGv',
      align: 'center',
      // width: 120,
    },
    {
      title: 'Số giờ NCKH',
      dataIndex: 'soGioNCKH',
      align: 'center',
      // width: 200,
    },

    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: DanhMucUser.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Quản lý giờ NCKH"
      hascreate
      modelName="danhmucusergionckh"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      Form={() => <FormGioNCKH getData={getData} />}
    />
  );
};

export default GioNCKHComponent;
