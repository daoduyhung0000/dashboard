import DebouncedSelect from '@/components/DebouncedSelect';
import FormBase from '@/components/Form';
import rules from '@/utils/rules';
import { InputNumber } from 'antd';
import FormItem from 'antd/es/form/FormItem';
import { useModel } from 'umi';

const FormTienTamUng = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('danhmucusertientamung');

  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <>
      <FormItem label="Năm học" name={'nam'} initialValue={record?.nam} rules={[...rules.required]}>
        <InputNumber
          min={2010}
          max={new Date().getFullYear() + 2}
          style={{ width: '100%' }}
          placeholder="Năm học"
        />
      </FormItem>

      <DebouncedSelect initialValue={record?.maGv} />

      <FormItem
        label="Số tiền tạm ứng"
        name={'soTienTamUng'}
        initialValue={record?.soTienTamUng}
        rules={[...rules.required]}
      >
        <InputNumber
          formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
          min={0}
          style={{ width: '100%' }}
          placeholder="Số tiền tạm ứng"
        />
      </FormItem>
    </>
  );

  return (
    <FormBase
      modelName="danhmucusertientamung"
      otherProps={{ onFinish: onSubmit, labelCol: { span: 24 } }}
    >
      {content}
    </FormBase>
  );
};

export default FormTienTamUng;
