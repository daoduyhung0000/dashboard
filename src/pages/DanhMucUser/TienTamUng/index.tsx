import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { IColumn } from '@/utils/interfaces';
import { currencyFormat } from '@/utils/utils';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import FormTienTamUng from './components/Form';

const GioNCKHComponent = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('danhmucusertientamung');

  const getData = () => {
    getModel(undefined, 'pageable');
  };
  const handleEdit = (record: DanhMucUser.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<DanhMucUser.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Năm học',
      dataIndex: 'nam',
      align: 'center',
    },
    {
      title: 'Giảng viên',
      dataIndex: 'maGv',
      align: 'center',
      // width: 120,
    },
    {
      title: 'Số tiền tạm ứng',
      dataIndex: 'soTienTamUng',
      align: 'center',
      render: (val) => currencyFormat(val),
      // width: 200,
    },

    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: DanhMucUser.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Quản lý tiền tạm ứng"
      hascreate
      modelName="danhmucusertientamung"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      Form={() => <FormTienTamUng getData={getData} />}
    />
  );
};

export default GioNCKHComponent;
