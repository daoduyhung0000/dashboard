import { Pie } from '@ant-design/plots';

const Chart = (props: {data: any, name: string}) => {

    const config = {
        appendPadding: 5,
        data: props.data,
        angleField: 'value',
        colorField: 'type',
        radius: 1,
        innerRadius: 0.7,
        autoFit: true,
        height: 316,
        label: {
          type: 'inner',
          offset: '-50%',
          content: '{value}',
          style: {
            textAlign: 'center',
            fontSize: 12,
          },
        },
        interactions: [
          {
            type: 'element-selected',
          },
          {
            type: 'element-active',
          },
        ],
      };
    return <Pie {...config} style={{background: 'white'}}/>
}

export default Chart