import { Column } from "@ant-design/plots"
import { useEffect } from "react"
import { useModel } from "umi"

const ColumnRange: React.FC = () => {
    const {getLike, likeInteractive} = useModel('dashboard')
    useEffect(() => {
        getLike()
    }, [])
    const config = {
        data: likeInteractive,
        xField: 'nam',
        yField: 'khoang',
        height: 315,
        isRange: true,
      };
      return <Column {...config} style={{backgroundColor: 'white'}}/>;
}
export default ColumnRange