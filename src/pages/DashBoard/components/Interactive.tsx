import { Card, Col, Typography } from "antd"

const {Title} = Typography
const Interactive = (props: {name: string, interactive: number}) => {
    return(
        <Col xs={24} sm={12} md={6} lg={24}>
        <Card style={{width: '100%', textAlign:'center', height:'150px'}}>
            <Title level={3}>{props.interactive}</Title>
            <Title level={5}>{props.name}</Title>
        </Card>
        </Col>
    )
}

export default Interactive
