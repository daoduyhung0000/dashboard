import { TinyLine } from "@ant-design/plots";
import { useModel } from "umi"

const LikeLine: React.FC = () => {
    const {arrLike} = useModel('dashboard')
    const config = {
        height: 315,
        autoFit: true,
        data: arrLike,
        smooth: true,
      };
      return <TinyLine {...config} style={{backgroundColor: 'white'}}/>;
}

export default LikeLine