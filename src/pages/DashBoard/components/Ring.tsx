import { RingProgress } from "@ant-design/plots";
import { useModel } from "umi";

const Ring: React.FC = () => {
    const { trustFollowerPercent } = useModel('dashboard')
    const config = {
        height: 150,
        autoFit: true,
        percent: trustFollowerPercent,
        color: ['#5B8FF9', '#E8EDF3'],
      };
      return <RingProgress {...config} style={{backgroundColor: 'white'}}/>;
}

export default Ring