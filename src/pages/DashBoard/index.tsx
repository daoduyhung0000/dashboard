import { Card, Col, Row } from "antd"
import { useEffect } from "react"
import { useModel } from "umi"
import Interactive from "./components/Interactive"
import Chart from "./components/Chart"
import Title from "antd/lib/typography/Title"
import Ring from "./components/Ring"
import ColumnRange from "./components/ColumnRange"
import LikeLine from "./components/LikeLine"

const DashBoard: React.FC = () => {
    const {getAllDanhSach, danhSach, followersChart, subscribersChart, likeChart, getRank, rank} = useModel('dashboard')

    useEffect(()=>{
        getAllDanhSach()
        getRank()
    },[])

    const listInteractive = () => {
        return danhSach.map((data) => {
            return <Interactive name={`${data?.name} Likes`} interactive={data?.like} key={data?.id}/> 
        })
    }
    return (
        <Row gutter={[16, 16]}>
            <Col xs={24} sm={24} md={24} lg={4}>
                <Row gutter={[16,16]}>
                {listInteractive()}
                </Row>
            </Col>
            <Col xs={24} sm={24} md={24} lg={20}>
                <Row gutter={[16,16]}>
                    <Col xs={24} sm={12} md={12} lg={10}><Chart data={followersChart} name="Follower"/></Col>
                    <Col xs={24} sm={12} md={12} lg={10}><Chart data={subscribersChart} name="Subscribers"/></Col>
                    <Col xs={24} sm={24} md={24} lg={4}>
                    <Row gutter={[16,16]}>
                        <Col xs={24} sm={12} md={12} lg={24}>
                            <Card style={{height:'150px'}}>
                                <Title level={3}>{rank?.percent || 0}%</Title>
                                <Title level={5}>RANK</Title>
                            </Card>
                        </Col>
                        <Col xs={24} sm={12} md={12} lg={24}>
                            <Ring/>
                        </Col>
                    </Row>
                    </Col>
                    <Col xs={24} sm={12} md={12} lg={10}><ColumnRange/></Col>
                    <Col xs={24} sm={12} md={12} lg={7}><LikeLine/></Col>
                    <Col xs={24} sm={24} md={24} lg={7}><Chart data={likeChart} name="Like"/></Col>
                </Row>
            </Col>
        </Row>
    )
}

export default DashBoard