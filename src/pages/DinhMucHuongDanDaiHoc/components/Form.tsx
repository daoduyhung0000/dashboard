import FormBase from '@/components/Form';
import rules from '@/utils/rules';
import { Input, InputNumber } from 'antd';
import FormItem from 'antd/es/form/FormItem';
import { useModel } from 'umi';

const FormDinhMucHuongDanDaiHoc = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('dinhmuchuongdandaihoc');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <>
      <FormItem
        label="Loại hình"
        name={'loaiHinh'}
        initialValue={record?.loaiHinh}
        rules={[...rules.required, ...rules.text]}
      >
        <Input placeholder="Loại hình" />
      </FormItem>
      <FormItem
        label="Định mức CTTT"
        name={'cttt'}
        initialValue={record?.cttt}
        rules={[...rules.required]}
      >
        <InputNumber style={{ width: '100%' }} placeholder="Định mức CTTT" min={0} />
      </FormItem>
      <FormItem
        label="Định mức CLC"
        name={'clcOrDhnn'}
        initialValue={record?.clcOrDhnn}
        rules={[...rules.required]}
      >
        <InputNumber style={{ width: '100%' }} placeholder="Định mức CLC" min={0} />
      </FormItem>
      <FormItem
        label="Định mức tiêu chuẩn"
        name={'tieuChuan'}
        initialValue={record?.tieuChuan}
        rules={[...rules.required]}
      >
        <InputNumber style={{ width: '100%' }} placeholder="Định mức tiêu chuẩn" min={0} />
      </FormItem>
      <FormItem
        label="Định mức ngôn ngữ thương mại"
        name={'nntm'}
        initialValue={record?.nntm}
        rules={[...rules.required]}
      >
        <InputNumber style={{ width: '100%' }} placeholder="Định mức ngôn ngữ thương mại" min={0} />
      </FormItem>
    </>
  );

  return (
    <FormBase
      modelName="dinhmuchuongdandaihoc"
      otherProps={{ onFinish: onSubmit, labelCol: { span: 24 } }}
    >
      {content}
    </FormBase>
  );
};

export default FormDinhMucHuongDanDaiHoc;
