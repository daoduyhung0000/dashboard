import FormBase from '@/components/Form';
import { FormItem } from '@/components/FormItem';
import rules from '@/utils/rules';
import { Input, InputNumber } from 'antd';
import { useModel } from 'umi';

const FormDinhMucHuongDanKhac = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('dinhmuchuongdankhac');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <>
      <FormItem
        label="Loại hình"
        name={'loaiHinh'}
        initialValue={record?.loaiHinh}
        rules={[...rules.required, ...rules.text]}
      >
        <Input placeholder="Loại hình" />
      </FormItem>
      <FormItem
        label="Định mức"
        name={'dinhMuc'}
        initialValue={record?.cttt}
        rules={[...rules.required]}
      >
        <InputNumber style={{ width: '100%' }} placeholder="Định mức" min={0} />
      </FormItem>
    </>
  );

  return (
    <FormBase
      modelName="dinhmuchuongdankhac"
      otherProps={{ onFinish: onSubmit, labelCol: { span: 24 } }}
    >
      {content}
    </FormBase>
  );
};

export default FormDinhMucHuongDanKhac;
