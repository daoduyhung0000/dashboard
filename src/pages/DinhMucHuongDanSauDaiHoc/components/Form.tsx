import FormBase from '@/components/Form';
import { FormItem } from '@/components/FormItem';
import { EHinhThuc, ELoaiHinh, ETrinhDo } from '@/utils/constants';
import rules from '@/utils/rules';
import { Col, InputNumber, Row, Select } from 'antd';
import { useModel } from 'umi';

const FormDinhMucHuongDanSauDaiHoc = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('dinhmuchuongdansaudaihoc');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <Row gutter={[8, 0]}>
      <Col xs={24} sm={12}>
        <FormItem
          label="Trình độ"
          name={'trinhDo'}
          initialValue={record?.trinhDo}
          rules={[...rules.required, ...rules.text]}
        >
          <Select
            placeholder="Trình độ"
            options={Object.values(ETrinhDo).map((item) => ({ label: item, value: item }))}
          />
        </FormItem>
      </Col>
      <Col xs={24} sm={12}>
        <FormItem
          label="Loại hình"
          name={'loaiHinh'}
          initialValue={record?.loaiHinh}
          rules={[...rules.required, ...rules.text]}
        >
          <Select
            placeholder="Loại hình"
            options={Object.values(ELoaiHinh).map((item) => ({ label: item, value: item }))}
          />
        </FormItem>
      </Col>
      <Col xs={24} sm={12}>
        {' '}
        <FormItem
          label="Hình thức"
          name={'hinhThuc'}
          initialValue={record?.hinhThuc}
          rules={[...rules.required, ...rules.text]}
        >
          <Select
            placeholder="Hình thức"
            options={Object.values(EHinhThuc).map((item) => ({ label: item, value: item }))}
          />
        </FormItem>
      </Col>
      <Col xs={24} sm={12}>
        <FormItem
          label="Định mức trong nước"
          name={'trongNuoc'}
          initialValue={record?.trongNuoc}
          rules={[...rules.required]}
        >
          <InputNumber style={{ width: '100%' }} placeholder="Định mức trong nước" min={0} />
        </FormItem>
      </Col>
      <Col xs={24} sm={12}>
        <FormItem
          label="Định mức nước ngoài"
          name={'nuocNgoai'}
          initialValue={record?.nuocNgoai}
          rules={[...rules.required]}
        >
          <InputNumber style={{ width: '100%' }} placeholder="Định mức nước ngoài" min={0} />
        </FormItem>
      </Col>
      {/* <Col xs={24} sm={12}>
        <FormItem
          label="Quy đổi"
          name={'quyDoi'}
          initialValue={record?.quyDoi}
          rules={[...rules.required]}
        >
          <InputNumber style={{ width: '100%' }} placeholder="Quy đổi" min={0} />
        </FormItem>
      </Col> */}
    </Row>
  );

  return (
    <FormBase
      modelName="dinhmuchuongdansaudaihoc"
      otherProps={{ onFinish: onSubmit, labelCol: { span: 24 } }}
    >
      {content}
    </FormBase>
  );
};

export default FormDinhMucHuongDanSauDaiHoc;
