import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { DinhMuc } from '@/services/DinhMuc/typings';
import type { IColumn } from '@/utils/interfaces';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import FormDinhMuc from './components/Form';

const DinhMucHuongDanSauDaiHoc = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('dinhmuchuongdansaudaihoc');
  const getData = () => {
    getModel();
  };
  const handleEdit = (record: DinhMuc.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<DinhMuc.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Trình độ',
      dataIndex: 'trinhDo',
      // width: 100,
      align: 'center',
    },
    {
      title: 'Loại hình',
      dataIndex: 'loaiHinh',
      align: 'center',
    },
    {
      title: 'Hình thức',
      dataIndex: 'hinhThuc',
      align: 'center',
      // width: 120,
    },
    {
      title: 'Định mức trong nước',
      dataIndex: 'trongNuoc',
      align: 'center',
      width: 120,
    },
    {
      title: 'Định mức nước ngoài',
      dataIndex: 'nuocNgoai',
      align: 'center',
      width: 120,
    },
    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: DinhMuc.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Định mức hướng dẫn sau đại học"
      hascreate
      modelName="dinhmuchuongdansaudaihoc"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      Form={() => <FormDinhMuc getData={getData} />}
    />
  );
};

export default DinhMucHuongDanSauDaiHoc;
