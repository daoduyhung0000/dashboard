import FormBase from '@/components/Form';
import { FormItem } from '@/components/FormItem';
import { ELoaiChuongTrinh, ENgonNgu } from '@/utils/constants';
import rules from '@/utils/rules';
import { Col, Form, Input, InputNumber, Radio, Row, Select } from 'antd';
import { useModel } from 'umi';

const FormDonGia = () => {
  const { record, edit, putModel, postModel, heDaoTao } = useModel('dongiathanhtoan');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values);
    else postModel({ ...values, heDaoTao });
  };

  const content = (
    <Row gutter={[8, 0]}>
      <Col span={24}>
        <FormItem
          label="Chức danh"
          name={'chucDanh'}
          initialValue={record?.chucDanh}
          rules={[...rules.required]}
        >
          <Input placeholder="Chức danh" />
        </FormItem>
      </Col>
      <Col sm={12}>
        <FormItem
          label="Học hàm"
          name={'hocHam'}
          initialValue={record?.hocHam}
          rules={[...rules.required]}
        >
          <Input placeholder="Học hàm" />
        </FormItem>
      </Col>
      <Col sm={12}>
        <FormItem
          label="Học vị"
          name={'hocVi'}
          initialValue={record?.hocVi}
          rules={[...rules.required]}
        >
          <Input placeholder="Học vị" />
        </FormItem>
      </Col>
      <Col sm={12}>
        <FormItem
          label="Ngôn ngữ giảng dạy"
          name={'ngonNguGiangDay'}
          initialValue={record?.ngonNguGiangDay}
          rules={[...rules.required]}
        >
          <Select
            placeholder="Ngôn ngữ giảng dạy"
            options={Object.values(ENgonNgu).map((item) => ({ label: item, value: item }))}
          />
        </FormItem>
      </Col>
      <Col sm={12}>
        <FormItem
          label="Loại chương trình"
          name={'loaiChuongTrinh'}
          initialValue={record?.loaiChuongTrinh}
          rules={[...rules.required]}
        >
          <Select
            placeholder="Loại chương trình"
            options={Object.values(ELoaiChuongTrinh).map((item) => ({ label: item, value: item }))}
          />
        </FormItem>
      </Col>
      <Col sm={12}>
        <FormItem
          label="Là môn ngoại ngữ"
          name={'isMonNgoaiNgu'}
          initialValue={record?.isMonNgoaiNgu ?? false}
          rules={[...rules.required]}
        >
          <Radio.Group
            options={[
              { label: 'Có', value: true },
              { label: 'Không', value: false },
            ]}
          />
        </FormItem>
      </Col>
      <Col sm={12}>
        <Form.Item
          label="Đơn giá"
          name={'donGia'}
          initialValue={record?.donGia}
          rules={[...rules.required]}
        >
          <InputNumber
            formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            style={{ width: '100%' }}
            min={0}
            placeholder="Đơn giá"
          />
        </Form.Item>
      </Col>
    </Row>
  );

  return (
    <FormBase
      modelName="dongiathanhtoan"
      otherProps={{ onFinish: onSubmit, labelCol: { span: 24 } }}
    >
      {content}
    </FormBase>
  );
};

export default FormDonGia;
