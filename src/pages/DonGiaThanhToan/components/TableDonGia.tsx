import ToolColumn from '@/components/ToolColum';
import type { DonGiaThanhToan } from '@/services/DonGiaThanhToan/typings';
import type { IColumn } from '@/utils/interfaces';
import { currencyFormat } from '@/utils/utils';
import { DeleteOutlined, EditOutlined, PlusCircleFilled } from '@ant-design/icons';
import { Button, Input, Table } from 'antd';
import { useModel } from 'umi';

const TableDonGia = () => {
  const { setVisibleForm, setEdit, setRecord, deleteModel, danhSach, heDaoTao } =
    useModel('dongiathanhtoan');

  const handleCreate = () => {
    setRecord(undefined);
    setEdit(false);
    setVisibleForm(true);
  };

  const handleEdit = (record: DonGiaThanhToan.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id);
  };

  const data = danhSach.filter((item) => item.heDaoTao === heDaoTao);

  const column: IColumn<DonGiaThanhToan.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Chức danh',
      dataIndex: 'chucDanh',
      width: 150,
      align: 'center',
    },
    {
      title: 'Học hàm',
      dataIndex: 'hocHam',
      width: 150,
      align: 'center',
    },
    {
      title: 'Học vị',
      dataIndex: 'hocVi',
      width: 150,
      align: 'center',
    },
    {
      title: 'Ngôn ngữ giảng dạy',
      dataIndex: 'ngonNguGiangDay',
      width: 150,
      align: 'center',
    },
    {
      title: 'Loại chương trình',
      dataIndex: 'loaiChuongTrinh',
      width: 150,
      align: 'center',
    },
    {
      title: 'Là môn ngoại ngữ',
      dataIndex: 'isMonNgoaiNgu',
      width: 120,
      align: 'center',
      render: (val) => (val ? 'Có' : 'Không'),
    },
    {
      title: 'Đơn giá',
      dataIndex: 'donGia',
      width: 120,
      align: 'center',
      fixed: 'right',
      render: (val) => currencyFormat(val),
    },
    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: DonGiaThanhToan.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <>
      <Button
        onClick={handleCreate}
        icon={<PlusCircleFilled />}
        type="primary"
        style={{ marginBottom: 8 }}
      >
        Thêm mới
      </Button>
      <h4 style={{ display: 'inline-block', margin: '0 0px 8px 50px', float: 'right' }}>
        Tổng số:
        <Input
          style={{ width: '90px', fontWeight: 600, fontSize: 14, marginLeft: 10 }}
          value={data?.length ?? 0}
          readOnly
        />
      </h4>
      <Table
        rowKey={(record) => record._id}
        columns={column}
        size="small"
        dataSource={data.map((item, index) => ({ ...item, index: index + 1 }))}
      />
    </>
  );
};

export default TableDonGia;
