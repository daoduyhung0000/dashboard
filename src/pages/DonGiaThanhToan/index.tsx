import { EHeDaoTao } from '@/utils/constants';
import { Card, Modal, Tabs } from 'antd';
import { useEffect } from 'react';
import { useModel } from 'umi';
import FormDonGia from './components/Form';
import TableDonGia from './components/TableDonGia';

const DonGiaThanhToanComponent = () => {
  const { getModel, visibleForm, setVisibleForm, setHeDaoTao, heDaoTao } =
    useModel('dongiathanhtoan');

  useEffect(() => {
    getModel();
  }, []);

  const handleCancelModal = () => {
    setVisibleForm(false);
  };

  const handleTabChange = (key: any) => {
    setHeDaoTao(key);
  };

  return (
    <Card bodyStyle={{ paddingTop: 4 }} title="Quản lý đơn giá">
      <Tabs onChange={handleTabChange} activeKey={heDaoTao}>
        {Object.values(EHeDaoTao).map((item) => (
          <Tabs.TabPane tab={item} key={item}>
            <TableDonGia />
          </Tabs.TabPane>
        ))}
      </Tabs>
      <Modal
        destroyOnClose
        onCancel={handleCancelModal}
        bodyStyle={{ padding: 0 }}
        visible={visibleForm}
        footer={false}
      >
        <FormDonGia />
      </Modal>
    </Card>
  );
};

export default DonGiaThanhToanComponent;
