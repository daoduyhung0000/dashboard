import FormBase from '@/components/Form';
import rules from '@/utils/rules';
import { Form, Input } from 'antd';
import { useModel } from 'umi';

const FormNamThanhToan = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('namthanhtoan');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <>
      <Form.Item
        label="Năm thanh toán"
        name={'nam'}
        initialValue={record?.nam}
        rules={[...rules.required]}
      >
        <Input placeholder="Năm thanh toán" />
      </Form.Item>
    </>
  );

  return (
    <FormBase modelName="namthanhtoan" otherProps={{ onFinish: onSubmit }}>
      {content}
    </FormBase>
  );
};

export default FormNamThanhToan;
