import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { IColumn } from '@/utils/interfaces';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import FormNamThanhToan from './components/Form';

const NamThanhToan = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('namthanhtoan');
  const getData = () => {
    getModel(undefined, 'pageable');
  };
  const handleEdit = (record: NamThanhToan.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<NamThanhToan.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Năm thanh toán',
      dataIndex: 'nam',
      align: 'center',
    },
    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: NamThanhToan.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Năm thanh toán"
      hascreate
      modelName="namthanhtoan"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      Form={() => <FormNamThanhToan getData={getData} />}
    />
  );
};

export default NamThanhToan;
