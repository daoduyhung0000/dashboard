import FormBase from '@/components/Form';
import { FormItem } from '@/components/FormItem';
import rules from '@/utils/rules';
import { Col, Form, Input, InputNumber, Row } from 'antd';
import { useModel } from 'umi';

const FormNghiaVuMacDinh = (props: { getData: any }) => {
  const { record, edit, putModel, postModel } = useModel('nghiavumacdinh');
  const onSubmit = (values: any) => {
    if (edit) putModel(record?._id ?? '', values, props.getData);
    else postModel(values, props.getData);
  };

  const content = (
    <Row gutter={[8, 0]}>
      <Col span={24}>
        <FormItem
          label="Chức danh"
          name={'chucDanh'}
          initialValue={record?.chucDanh}
          rules={[...rules.required, ...rules.text]}
        >
          <Input placeholder="Chức danh" />
        </FormItem>
      </Col>
      <Col md={12}>
        <FormItem
          label="Học hàm"
          name={'hocHam'}
          initialValue={record?.hocHam}
          rules={[...rules.required, ...rules.text]}
        >
          <Input placeholder="Học hàm" />
        </FormItem>
      </Col>
      <Col md={12}>
        <FormItem
          label="Học vị"
          name={'hocVi'}
          initialValue={record?.hocVi}
          rules={[...rules.required, ...rules.text]}
        >
          <Input placeholder="Học vị" />
        </FormItem>
      </Col>
      <Col md={12}>
        <FormItem
          label="Giờ công tác khác"
          name={'congTacKhac'}
          initialValue={record?.congTacKhac}
          rules={[...rules.required]}
        >
          <InputNumber style={{ width: '100%' }} min={0} placeholder="Giờ công tác khác" />
        </FormItem>
      </Col>
      <Col md={12}>
        <FormItem
          label="Giờ giảng dạy"
          name={'giangDay'}
          initialValue={record?.giangDay}
          rules={[...rules.required]}
        >
          <InputNumber style={{ width: '100%' }} min={0} placeholder="Giờ giảng dạy" />
        </FormItem>
      </Col>
      <Col md={12}>
        <FormItem
          label="Giờ nghiên cứu khoa học"
          name={'nghienCuuKhoaHoc'}
          initialValue={record?.nghienCuuKhoaHoc}
          rules={[...rules.required]}
        >
          <InputNumber style={{ width: '100%' }} min={0} placeholder="Giờ nghiên cứu khoa học" />
        </FormItem>
      </Col>
      <Col md={12}>
        <Form.Item
          label="Giờ tự bồi dưỡng"
          name={'tuBoiDuong'}
          initialValue={record?.tuBoiDuong}
          rules={[...rules.required]}
        >
          <InputNumber style={{ width: '100%' }} min={0} placeholder="Giờ tự bồi dưỡng" />
        </Form.Item>
      </Col>
    </Row>
  );

  return (
    <FormBase
      modelName="nghiavumacdinh"
      otherProps={{ onFinish: onSubmit, labelCol: { span: 24 } }}
    >
      {content}
    </FormBase>
  );
};

export default FormNghiaVuMacDinh;
