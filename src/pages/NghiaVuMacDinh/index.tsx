import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { IColumn } from '@/utils/interfaces';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
import FormNghiaVuMacDinh from './components/Form';

const NghiaVuMacDinh = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('nghiavumacdinh');
  const getData = () => {
    getModel(undefined, 'pageable');
  };
  const handleEdit = (record: NghiaVuMacDinh.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<NghiaVuMacDinh.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Chức danh',
      dataIndex: 'chucDanh',
      align: 'center',
      width: 100,
    },
    {
      title: 'Học hàm',
      dataIndex: 'hocHam',
      width: 80,
      align: 'center',
    },
    {
      title: 'Học vị',
      dataIndex: 'hocVi',
      width: 80,
      align: 'center',
    },
    {
      title: 'Giờ công tác khác',
      dataIndex: 'congTacKhac',
      width: 80,
      align: 'center',
    },
    {
      title: 'Giờ giảng dạy',
      dataIndex: 'giangDay',
      width: 80,
      align: 'center',
    },
    {
      title: 'Giờ nghiên cứu khoa học',
      dataIndex: 'nghienCuuKhoaHoc',
      width: 80,
      align: 'center',
    },

    {
      title: 'Giờ tự bồi dưỡng',
      dataIndex: 'tuBoiDuong',
      width: 80,
      align: 'center',
    },
    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: NghiaVuMacDinh.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Nghĩa vụ mặc định"
      hascreate
      modelName="nghiavumacdinh"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      Form={() => <FormNghiaVuMacDinh getData={getData} />}
    />
  );
};

export default NghiaVuMacDinh;
