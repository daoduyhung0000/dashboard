import TableBase from '@/components/Table';
import ToolColumn from '@/components/ToolColum';
import type { RaDeCoiThiChamThi } from '@/services/RaDeCoiThiChamThi/typings';
import type { IColumn } from '@/utils/interfaces';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useModel } from 'umi';
// import FormCongViec from './components/Form';

const CongViec = () => {
  const {
    loading,
    page,
    limit,
    condition,
    setEdit,
    setVisibleForm,
    setRecord,
    getModel,
    deleteModel,
  } = useModel('radecoithichamthi');
  const getData = () => {
    getModel(undefined, 'pageable');
  };
  const handleEdit = (record: RaDeCoiThiChamThi.Record) => {
    setEdit(true);
    setRecord(record);
    setVisibleForm(true);
  };

  const handleDelete = (id: string) => {
    deleteModel(id, getData);
  };

  const columns: IColumn<RaDeCoiThiChamThi.Record>[] = [
    {
      title: 'STT',
      dataIndex: 'index',
      width: 80,
      align: 'center',
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      align: 'center',
    },
    {
      title: 'Giá trị',
      dataIndex: 'value',
      align: 'center',
    },
    {
      title: 'Loại',
      dataIndex: 'type',
      align: 'center',
    },
    {
      title: 'Thao tác',
      align: 'center',
      width: 120,
      render: (record: RaDeCoiThiChamThi.Record) => (
        <ToolColumn
          type="normal"
          hasEdit={{
            icon: <EditOutlined />,
            onClick: () => handleEdit(record),
          }}
          hasDelete={{
            handleDelete: () => handleDelete(record._id),
            props: {
              icon: <DeleteOutlined />,
              type: 'primary',
              danger: true,
            },
          }}
        />
      ),
    },
  ];

  return (
    <TableBase
      title="Danh mục thanh toán"
      hascreate
      modelName="RaDeCoiThiChamThi"
      dependencies={[page, limit, condition]}
      getData={getData}
      loading={loading}
      columns={columns}
      // Form={() => <FormCongViec getData={getData} />}
    />
  );
};

export default CongViec;
