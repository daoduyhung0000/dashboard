import type { Props } from "@/components/Table";
import TableBase from "@/components/Table"
import moment from "moment";
import { memo } from "react"
import { useModel } from "umi"


const Table: React.FC = () => {
    const {namActive, activeApi} = useModel('thanhtoan')
    const {page, limit, getModel, loading} = useModel('chuakhoitao')
    let begin = (page * limit) - limit
    console.log('re-render')
    const tableData: any = [
        {
            title: 'STT',
            key: 'stt',
            align: 'center',
            dataIndex: 'key',
            width: 100,
            render: () => {
                return <div>{begin = begin + 1}</div>
            }
        },
        {
            title: 'Họ và tên',
            key: 'hoVaTen',
            dataIndex: 'name',
            align: 'center',
            width: 300
        },
        {
            title: 'Mã giảng viên',
            key: 'maGiangVien',
            dataIndex: 'ma_dinh_danh',
            align: 'center',
            width: 200
        },
        {
            title: 'Ngày Sinh',
            key: 'ngaySinh',
            dataIndex: 'ngay_sinh',
            align: 'center',
            width: 200,
            render: (data: string) =>{
                return data ? <div>{moment(data).format('DD-MM-YYYY')}</div> : 'Chưa cập nhật'
            }
        },
        {
            title: 'Chức vụ',
            key: 'chucVu',
            dataIndex: 'chuc_vu',
            align: 'center',
            width: 350
        },
        {
            title: 'Thao tác',
            key: 'thaoTac',
            align: 'center',
            fixed: 'right',
            width: 100
        }
    ]

    const tableProperties: Props = {
        modelName: 'chuakhoitao',
        columns: tableData,
        title: 'Danh sách giảng viên',
        getData: () => {
            if(namActive !==0 ) getModel({nam: namActive}, activeApi, page, limit)
        },
        dependencies: [limit, page, namActive, activeApi],
        loading: loading,
    }
    return <TableBase {...tableProperties}/>
}

export default memo(Table)