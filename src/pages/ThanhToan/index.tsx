import {LeftOutlined, LoadingOutlined, RightOutlined} from "@ant-design/icons"
import { Select, Spin, Tabs } from "antd"
import React, { useEffect, useRef} from "react"
import { useModel } from "umi"
import Table from "./components/table"
import style from './index.module.less'

const {Option} = Select
const { TabPane } = Tabs

const ThanhToan: React.FC = () => {
    const triggerElement = useRef<HTMLDivElement>(null) 
    const {
        danhSachNamThanhToan,
        checkNamThanhToanSuccess,
        getDanhSachNamThanhToan,
        isNamThanhToanErr,
        setNamActive,
        namActive,
        setActiveApi
    } = useModel('thanhtoan')

    useEffect(() => {
        getDanhSachNamThanhToan()
    },[])
    const listNamThanhToan = () => {
        if(checkNamThanhToanSuccess){
            return danhSachNamThanhToan.map(list => {
                return <Option value={list?.id} key={list?.id}>Năm học: {list?.nam}</Option>
            })
        } 
    }

    const handleScroll = (type: string) => {
        if(triggerElement.current){
            const elementHasScroll = triggerElement.current.querySelector('.ant-tabs-nav-list') as HTMLElement
            const listElement = triggerElement.current.querySelectorAll('.ant-tabs-tab') as NodeListOf<HTMLElement>
            const ratio = elementHasScroll.scrollWidth / Math.round(elementHasScroll.scrollWidth /  elementHasScroll.clientWidth)
            // Lấy tỉ lệ với khối 
            const listLocation: number[] = []
            listElement.forEach(ele => {
                if(type === "LEFT"){
                    listLocation.push(ele.offsetLeft + ele.offsetWidth)
                    listLocation.unshift(0)
                    
                }else{
                    listLocation.unshift(ele.offsetLeft + ele.offsetWidth)
                    listLocation.push(0)
                }
            })
            // tạo mạng chứa toạ độ cố định của các tab
            if(type === 'RIGHT'){
                const point = listLocation.find(value => value < elementHasScroll.scrollLeft + ratio )
                // lấy toạ độ tab gần nhất
                elementHasScroll.scroll({left: point, behavior:'smooth'})
            }else{
                const point = listLocation.find(value => value > elementHasScroll.scrollLeft - ratio )
                // lấy toạ độ tab gần nhất
                elementHasScroll.scroll({left: point, behavior:'smooth'})     
            }
        }
    }

    return (
        <div className={style.thanhToanWrap}>
            <div 
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    }}
                >
                <Select
                    showSearch
                    className={style.select}
                    disabled={!checkNamThanhToanSuccess}
                    notFoundContent={isNamThanhToanErr ? 'Vui lòng tải lại trang' : 'Không có dữ liệu'}
                    value={namActive}
                    optionFilterProp="children"
                    onChange={(value) => setNamActive(value)}
                >
                    {listNamThanhToan()}
                </Select>
                <Spin 
                    style={{
                        display: checkNamThanhToanSuccess ? 'none' : 'block',
                        margin: '0 1rem'
                        }} 
                        indicator={<LoadingOutlined spin/>}
                />
            </div>
            <div style={{
                display:'flex',
                alignItems: 'center',
                marginBottom: '1rem',
                }}
                ref={triggerElement}
            >
            <LeftOutlined 
                className={style.control} 
                onClick={() => {handleScroll("LEFT")}}
                />
            <Tabs 
                defaultActiveKey="/pageable/chua-khoi-tao" 
                className={style.menu}
                onChange={(activeKey) => setActiveApi(activeKey)}
                >
                <TabPane tab="Chưa khởi tạo" key="/pageable/chua-khoi-tao"/>
                <TabPane tab="Chưa khoá" key="/pageable/chua-khoi-tao1"/>
                <TabPane tab="Đã khoá" key="/pageable/chua-khoi-tao2"/>
                <TabPane tab="Đã khoá tạm ứng" key="/pageable/chua-khoi-tao3"/>
                <TabPane tab="Đã tiếp nhận" key="/pageable/chua-khoi-tao4"/>
                <TabPane tab="Không tiếp nhận" key="/pageable/chua-khoi-tao5"/>
            </Tabs>
            <RightOutlined className={style.control}  onClick={() => {handleScroll("RIGHT")}}/>
            </div>
            <Table/>
        </div>
    )
}

export default ThanhToan