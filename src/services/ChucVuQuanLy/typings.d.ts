declare module ChucVuQuanLy {
  export interface Record {
    _id: string;
    id: string;
    ten: string;
    tinhToanBoGioGiang: boolean;
    dinhMuc: number;
  }
}
