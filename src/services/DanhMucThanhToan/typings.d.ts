declare module DanhMucThanhToan {
  export interface Record {
    _id: string;
    name: string;
    type: string;
    value: string;
  }
}
