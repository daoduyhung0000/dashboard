declare module DanhMucUser {
  export interface Record {
    _id: string;
    maGv: string;
    nam: number;
    tenChucVu: string;
    soGioNCKH: number;
    soTienTamUng: number;
  }
}
