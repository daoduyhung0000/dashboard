import axios from "axios";

export async function getAllSocialInteractive() {
    return axios.get('https://62ffaf9c34344b6431ff3b39.mockapi.io/get/SocialInteractive')
}

export async function getSocialValue(){
    return axios.get('https://62ffaf9c34344b6431ff3b39.mockapi.io/get/rank')
}

export async function getLikeInteractive(){
    return axios.get('https://62ffaf9c34344b6431ff3b39.mockapi.io/get/Like')
}