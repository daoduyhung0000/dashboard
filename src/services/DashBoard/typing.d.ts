declare module SocialInteractive {
    export interface Record {
        createdAt: string
        name: string
        like: number,
        followers: number,
        subscribers: number,
        trustFollower: number,
        id: number
    }
}

declare module Rank {
    export interface Record {
        type: string,
        percent: number,
        id: number
    }
}

declare module Like {
    export interface Record {
        createdAt: string,
        khoang: numer[]
        nam: number,
        id: number
    }
}