import type { EHinhThuc, ELoaiHinh, ETrinhDo } from '@/utils/constants';

declare module DinhMuc {
  export interface Record {
    _id: string;
    dinhMuc: number;
    loaiHinh: ELoaiHinh | string;
    nuocNgoai: number;
    trongNuoc: number;
    trinhDo: ETrinhDo;
    hinhThuc: EHinhThuc;
    quyDoi: number;
    tieuChuan: number;
    cttt: number;
    clcOrDhnn: number;
    nntm: number;
  }
}
