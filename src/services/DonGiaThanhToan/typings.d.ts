import type { EHeDaoTao, ELoaiChuongTrinh } from '@/utils/constants';

declare module DonGiaThanhToan {
  export interface Record {
    _id: string;
    chucDanh: string;
    hocHam: string;
    hocVi: string;
    heDaoTao: EHeDaoTao;
    loaiChuongTrinh: ELoaiChuongTrinh;
    ngonNguGiangDay: string;
    isMonNgoaiNgu: boolean;
    donGia: number;
  }
}
