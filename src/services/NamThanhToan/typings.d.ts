declare module NamThanhToan {
  export interface Record {
    createdAt: string;
    id: string;
    nam: string;
    updatedAt: string;
    _id: string;
  }
}
