declare module NghiaVuMacDinh {
  export interface Record {
    chucDanh: string;
    congTacKhac: number;
    giangDay: number;
    hocHam: string;
    hocVi: string;
    nghienCuuKhoaHoc: number;
    tuBoiDuong: number;
    _id: string;
  }
}
