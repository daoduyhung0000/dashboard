import type { EHeDaoTao, ELoaiCongViec, ELoaiThi } from '@/utils/constants';

declare module RaDeCoiThiChamThi {
  export interface Record {
    _id: string;
    donViThanhToan: string;
    donViTinh: string;
    fillAmount: boolean;
    gioChuan: number;
    heDaoTao: EHeDaoTao;
    heSo: number;
    heSoThanhToan: number;
    loaiCongViec: ELoaiCongViec;
    loaiThi: ELoaiThi;
    mucThanhToan: number;
    summaryGroup: string;
    tenCongViec: string;
  }
}
