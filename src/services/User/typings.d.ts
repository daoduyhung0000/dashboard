import type { ESystemRole, Gender } from '@/utils/constants';

declare module User {
  export interface Record {
    id: number;
    partner_id: [number, string];
    name: string;
    ma_dinh_danh: string;
    ngay_sinh: string;
    gioi_tinh: Gender;
    vai_tro: ESystemRole;
    so_dien_thoai: string;
    dia_chi_hien_nay: string;
    don_vi_id: [number, string];
    ten_don_vi: string;
    email_to_chuc: string;
  }
}
