import type { ESystemRole, Gender } from '@/utils/constants';

declare module Login {
  export interface User {
    uid: number;
    partner_id: 27381;
    ho_ten: string;
    ma_dinh_danh: string;
    email: string;
    gioi_tinh: string;
    vai_tro: string;
  }

  export interface RegisterPayload {
    email: string;
    password: string;
    hoDem: string;
    ten: string;
    ngaySinh: string;
    gioiTinh: string;
    soDienThoai: string;
  }

  export interface Profile {
    email: string;
    profile: {
      dateOfBirth: string;
      firstname: string;
      gender: Gender;
      lastname: string;
      _id: string;
      avatar: string;
      ngaySinh: string;
      soDienThoai: string;
      diaChi: string;
    };
    systemRole: ESystemRole;
    username: string;
    _id: string;
  }

  export interface Record {
    user: User;
    accessTokens: { vai_tro: string; token: string }[];
  }
}
