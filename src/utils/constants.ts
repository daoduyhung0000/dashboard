export enum ESystemRole {
  admin = 'Admin',
  nhan_vien = 'nhan_vien',
  sinh_vien = 'sinh_vien',
}

export enum Role {
  nhan_vien = 'Cán bộ, giảng viên',
  sinh_vien = 'Sinh viên',
  Admin = 'Quản trị viên hệ thống',
  quan_tri = 'Quản trị viên đơn vị',
}

export const MapKeyRole = {
  Admin: 'Admin',
  QuanTriVien: 'Quản trị viên',
  ThiSinh: 'Thí sinh',
  PhuHuynh: 'Phụ huynh',
  Guest: 'Khách',
};

export enum Gender {
  Nam = 'Nam',
  Nu = 'Nữ',
  Khac = 'Khác',
}

export enum ETrinhDo {
  THAC_SY = 'Thạc sỹ',
  TIEN_SY = 'Tiến sỹ',
}

export enum ELoaiHinh {
  CHUYEN_DE = 'Chuyên đề',
  LUAN_VAN = 'Luận văn',
  LUAN_AN = 'Luận án',
}

export enum EHinhThuc {
  DOC_LAP = 'Độc lập',
  HD1 = 'HD1',
  HD2 = 'HD2',
}

export enum EHeDaoTao {
  DAI_HOC_CHINH_QUY = 'Đại học Chính quy',
  DAI_HOC_PHI_CHINH_QUY = 'Đại học Phi chính quy',
  SAU_DAI_HOC = 'Sau Đại học',
  LIEN_KET_QUOC_TE = 'Liên kết quốc tế',
}

export enum ELoaiChuongTrinh {
  CHUONG_TRINH_TIEU_CHUAN = 'Chương trình tiêu chuẩn',
  CHUONG_TRINH_CHAT_LUONG_CAO = 'Chương trình chất lượng cao',
  CHUONG_TRINH_TIEN_TIEN = 'Chương trình tiên tiến',
  THAC_SY = 'Thạc sỹ',
  TIEN_SY = 'Tiến sỹ',
}

export enum ENgonNgu {
  TIENG_VIET = 'Tiếng Việt',
  TIENG_ANH = 'Tiếng Anh',
}

export enum ELoaiThi {
  THI_GIUA_KY = 'Thi giữa kỳ',
  THI_KET_THUC_HOC_PHAN = 'Thi kết thúc học phần',
  THI_TOT_NGHIEP = 'Thi tốt nghiệp',
  THI_TRAC_NGHIEM_TREN_MAY_TINH = 'Thi trắc nghiệm trên máy tính',
}

export enum ELoaiCongViec {
  RA_DE = 'Ra đề',
  COI_THI = 'Coi thi',
  CHAM_THI = 'Chấm thi',
}

export const Setting = {
  navTheme: 'dark',
  primaryColor: '#CC0D00',
  layout: 'mix',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  title: 'Ecount FTU',
  pwa: false,
  logo: '/favicon.ico',
  iconfontUrl: '',
};
